{{template "admin" .}}
{{ define "title"}}
    Dashboard | Reservations Show
{{end}}
{{define "page-title"}}
    Reservations Show
{{end}}
{{define "content" }}
    <div class="col-md-12">
        {{$res:= index .Data "reservation"}}
        {{$src:= index .StrMap "src"}}
        <p>
            <strong>Arrival:</strong> {{humanDate $res.StartDate}}
        </p>
        <p>
            <strong>Departure:</strong> {{humanDate $res.EndDate}}
        </p>
        <p>
            <strong>Room:</strong> {{$res.Room.RoomName}}
        </p>

        <form action="/admin/reservations/{{$src}}/{{$res.ID}}/show" method="post" class="" novalidate>
            <input type="hidden" name="csrf_token" value="{{.CSRFToken}}">
            <input type="hidden" name="year" value="{{ index .StrMap "year"}}">
            <input type="hidden" name="month" value="{{ index .StrMap "month"}}">
            <div class="form-group mt-2">
                <label for="firstName">First Name:</label>
                {{with .Form.Errors.Get "firstName"}}
                    <label class="text-danger">{{.}}</label>
                {{end}}
                <input
                        type="text"
                        name="firstName"
                        id="firstName"
                        class="form-control {{with .Form.Errors.Get "firstName"}} is-invalid {{end}}"
                        required
                        autocomplete="off"
                        value="{{$res.FirstName}}"
                />
            </div>
            <div class="form-group">
                <label for="lastName">Last Name:</label>
                {{with .Form.Errors.Get "lastName"}}
                    <label class="text-danger">{{.}}</label>
                {{end}}
                <input
                        type="text"
                        name="lastName"
                        id="lastName"
                        class="form-control {{with .Form.Errors.Get "lastName"}} is-invalid {{end}}"
                        required
                        autocomplete="off"
                        value="{{$res.LastName}}"
                />
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                {{with .Form.Errors.Get "email"}}
                    <label class="text-danger">{{.}}</label>
                {{end}}
                <input
                        type="email"
                        name="email"
                        id="email"
                        class="form-control {{with .Form.Errors.Get "email"}} is-invalid {{end}}"
                        required
                        autocomplete="off"
                        value="{{$res.Email}}"
                />
            </div>
            <div class="form-group">
                <label for="phone">Phone:</label>
                {{with .Form.Errors.Get "phone"}}
                    <label class="text-danger">{{.}}</label>
                {{end}}
                <input
                        type="tel"
                        name="phone"
                        id="phone"
                        class="form-control {{with .Form.Errors.Get "phone"}} is-invalid {{end}}"
                        required
                        autocomplete="off"
                        value="{{$res.Phone}}"
                />
            </div>
            <div class="float-left">
                <input
                        type="submit"
                        value="Save!"
                        class="btn btn-success mt-2"
                />
                {{if eq $src "cal" }}
                    {{/*we use history here to go back*/}}
                    <a href="#!" class="btn btn-warning mt-2" onclick="window.history.go(-1)">Cancel</a>
                {{else}}
                    <a href="/admin/reservations-{{$src}}" class="btn btn-warning mt-2">Cancel</a>
                {{end}}

                {{if eq $res.Processed 0}}
                    <a href="#!" class="btn btn-info mt-2" onclick="processRes({{$res.ID}})">Mark As Processed!</a>
                {{end}}
            </div>
            <div class="float-right">
                <a href="#!" class="btn btn-danger mt-2" onclick="deleteRes({{$res.ID}})">Delete Reservation!</a>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
{{end}}
{{define "js"}}
    {{/*    callback what will happen if someone click the ok button*/}}
    <script>
        {{$src:= index .StrMap "src"}}
        function deleteRes(id) {
            attention.custom({
                icon: 'warning',
                msg: 'Are You Sure?',
                callback: function (result) {
                    if (result !== false) {
                        window.location.href = "/admin/process/delete/{{$src}}/" + id + "/do?y={{index .StrMap "year"}}&m={{index .StrMap "month"}}";
                    }
                },
            })
        }

        function processRes(id) {
            //fires an sweet alert
            attention.custom({
                icon: 'warning',
                msg: 'Are You Sure?',
                callback: function (result) {
                    if (result !== false) {
                        window.location.href = "/admin/process/reservation/{{$src}}/" + id + "/do?y={{index .StrMap "year"}}&m={{index .StrMap "month"}}";
                    }
                },
            })

        }
    </script>
{{end}}